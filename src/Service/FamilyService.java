package Service;

import DAO.CollectionFamilyDao;
import DAO.FamilyDao;
import Entity.*;

import java.time.Instant;
import java.time.Year;
import java.time.ZoneId;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService { // CONTAINS ALL LOGIC, IF IS POSSIBLE THEN PROCEEDS TO DAO AND FURTHER, IF NOT RETURNS BACK TO CONTROLLER

    private final FamilyDao familyDao = new CollectionFamilyDao();

    public int count() {
        return familyDao.count();
    }

    public List<Family> getAllFamilies() {

        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyDao.getAllFamilies().forEach(family -> System.out.println(family.toString()));
    }

    public void getFamiliesBiggerThan(int number) {
        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() > number).
                forEach(family -> System.out.println(family.toString()));
    }

    public void getFamiliesLessThan(int number) {
        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() < number).
                forEach(family -> System.out.println(family.toString()));
    }

    public void countFamiliesWithMemberNumber(int number) {
        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() == number).
                forEach(family -> System.out.println(family.toString()));
    }

    public Family getFamilyByIndex(int index) {
        try {
            System.out.println(familyDao.getFamilyByIndex(index));
            return familyDao.getFamilyByIndex(index);
        } catch (Exception e) {
            System.out.println("null");
            return null;
        }
    }

    public boolean deleteFamilyByIndex(int index) {
        boolean deleted;
        try {
            familyDao.deleteFamily(index);
            System.out.println("true");
            deleted = true;
        } catch (Exception e) {
            System.out.println("false");
            deleted = false;
        }
        return deleted;
    }

    public boolean deleteFamilyByFamily(Family family) {
        boolean deleted;
        try {
            if (familyDao.getAllFamilies().contains(family)) {
                familyDao.deleteFamily(family);
                System.out.println("true");
                deleted = true;
            } else {
                System.out.println("false");
                deleted = false;
            }
        } catch (Exception e) {
            System.out.println("false");
            deleted = false;
        }
        return deleted;
    }

    public void saveFamily(Family family) {
        if (familyDao.getAllFamilies().contains(family)) {
            int permanentIndex = familyDao.getFamilyIndex(family);
            familyDao.deleteFamily(familyDao.getFamilyIndex(family));
            familyDao.saveFamilyByIndex(permanentIndex, family);
        } else
            familyDao.saveFamily(family);
    }

    public boolean createNewFamily(Human mother, Human father) {
        return familyDao.saveFamily(new Family(mother, father));
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        int index = familyDao.getFamilyIndex(family);
        if (boyName != null && girlName == null) {
            familyDao.getFamilyByIndex(index).addChild(new Man(boyName, familyDao.getFamilyByIndex(index).getFather().getSurname(), null, family.getPet()));
        } else if (boyName == null && girlName != null) {
            familyDao.getFamilyByIndex(index).addChild(new Woman(girlName, familyDao.getFamilyByIndex(index).getFather().getSurname(), null, family.getPet()));
        } else if (boyName != null && girlName != null) {
            familyDao.getFamilyByIndex(index).addChild(new Man(boyName, familyDao.getFamilyByIndex(index).getFather().getSurname(), null, family.getPet()));
            familyDao.getFamilyByIndex(index).addChild(new Woman(girlName, familyDao.getFamilyByIndex(index).getFather().getSurname(), null, family.getPet()));
        } else {
            System.out.println("Incorrect Input Data, Child/Children was/were not born!");
        }
        return familyDao.getFamilyByIndex(index);
    }

    public Family adoptChild(Family family, Human adoptedChild) {
        int index = familyDao.getFamilyIndex(family);
        familyDao.getFamilyByIndex(index).addChild(adoptedChild);
        return familyDao.getFamilyByIndex(index);
    }

    public void deleteAllChildrenOlderThan(int number) {

        for (Family family : getAllFamilies()) {
            List<Human> humanList = family.getChildrenList().stream().filter(human -> Year.now().getValue() -
                    Instant.ofEpochMilli(human.getBirthdate()).atZone(ZoneId.systemDefault()).toLocalDate().getYear() > number).collect(Collectors.toList());
            humanList.forEach(family::deleteChild);
        }
    }

    public Family getFamilyById(int id) {
        try {
            System.out.println(familyDao.getFamilyByIndex(id));
            return familyDao.getFamilyByIndex(id);
        } catch (Exception e) {
            System.out.println("No matches found.");
            return null;
        }
    }

    public Set<Pet> getPets(int index) {
        try {
            return familyDao.getFamilyByIndex(index).getPetList();
        } catch (Exception e) {
            System.out.println("No matches found.");
            return null;
        }
    }

    public void addPet(int index, Pet pet) {
        try {
            familyDao.getFamilyByIndex(index).setPet(pet);
        } catch (Exception e) {
            System.out.println("No matches found.");
        }
    }

}


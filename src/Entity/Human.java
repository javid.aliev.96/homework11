package Entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class Human {
    public Map schedule = new LinkedHashMap();
    private String name;
    private String surname;
    private long birthdate;
    private int iq;
    private Family family;

    public Human(String name, String surname, String birthdate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthdate = StringDateToLong(birthdate);
        this.iq = iq;
    }

    public Human(String name, String surname, String birthdate) {
        this.name = name;
        this.surname = surname;
        this.birthdate = StringDateToLong(birthdate);
    }

    public Human() {
    }

    public long getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(long birthdate) {
        this.birthdate = birthdate;
    }

    public void scheduleGetter() {
        System.out.println("The schedule is: " + this.schedule.toString());
    }

    public void scheduleSetter(String dayOfWeek, String tasks) {
        this.schedule.put(dayOfWeek, tasks);
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getSurname() {

        return surname;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }


    public int getIq() {

        return iq;
    }

    public void setIq(int iq) {

        this.iq = iq;
    }

    public Map<DayOfWeek, String> getSchedule() {

        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {

        this.schedule = schedule;
    }

    public Family getFamily() {

        return family;
    }

    public void setFamily(Family family) {

        this.family = family;
    }

    public void greetPet() {
        System.out.println("Hello, " + family.getPet().getNickname());
    }

    public void describePet() {
        if (family.getPet().getTrickLevel() > 50)
            System.out.println("I have a " + family.getPet().getSpecies() + ", he is " + family.getPet().getAgeOfPet() + " years old, he is very sly.");
        else {
            System.out.println("I have a " + family.getPet().getSpecies() + ", he is " + family.getPet().getAgeOfPet() + " years old, he is almost not sly.");
        }
    }

    public boolean feedPet(boolean time) {
        boolean feeding = false;
        Random random = new Random(101);
        if (time == true) {
            feeding = true;
        } else if (family.getPet().getTrickLevel() >= random.nextInt()) {
            System.out.println("Hm...I will feed " + family.getPet().getNickname());
            feeding = true;
        } else {
            System.out.println("I think " + family.getPet().getNickname() + " is not hungry.");
            feeding = false;
        }
        return feeding;
    }

    private long StringDateToLong(String birthdate) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyy");
        long date;
        try {
            date = df.parse(birthdate).getTime();
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String describeAge() {
        Instant instant = Instant.ofEpochMilli(birthdate);
        LocalDate localBirth = instant.atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate localDate = LocalDate.now();
        Period difference = Period.between(localBirth, localDate);

        return "Exact duration of " + this.name + "'s" + " life is: "
                + difference.getYears() + " years, "
                + difference.getMonths() + " months, "
                + difference.getDays() + " days.";
    }

    public String dateToString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return simpleDateFormat.format(this.birthdate);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name= " + name + '\'' +
                ", surname= " + surname + '\'' +
                ", birthdate= " + dateToString() +
                ", iq= " + iq +
                ", schedule= " + schedule.toString() +
                '}';
    }
}

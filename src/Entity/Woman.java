package Entity;

public final class Woman extends Human {
    private final Pet pet;

    public Woman(String name, String surname, String birthdate, Pet pet) {
        super(name, surname, birthdate);
        this.pet = pet;
    }

    @Override
    public void scheduleSetter(String dayOfWeek, String tasks) {
        super.scheduleSetter(dayOfWeek, tasks);
    }

    @Override
    public void scheduleGetter() {
        super.scheduleGetter();
    }

    public void whine() {
        System.out.println("We need to talk");
    }


    @Override
    public void greetPet() {
        System.out.println("Hello my dear " + this.pet.getNickname() + " I missed you a lot!");
    }
}

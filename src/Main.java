import Controllers.FamilyController;
import Entity.Family;
import Entity.Human;

public class Main {

    private final static FamilyController familyController = new FamilyController();

    public static void main(String[] args) {


        Human test1 = new Human("TestMother", "Homework10", "18/02/1970", 99);
        Human test2 = new Human("TestFather", "Homework10", "14/10/1959", 99);
        Human test3 = new Human("TestChild", "Homework10", "28/01/1996", 99);
        Human test4 = new Human("!", "!!", "04/04/2004", 66);
        Human test5 = new Human("Z", "ZZZ", "05/05/2005", 66);
        Human test6 = new Human("'asda", "asdasdasd", "07/07/2007", 66);
        Human test7 = new Human("ololool", "dghjkjh", "01/01/2001", 66);
        Family testFamily = new Family(0, "TestFamily");
        testFamily.setMother(test1);
        testFamily.setFather(test2);
        familyController.saveFamily(testFamily);
        familyController.adoptChild(testFamily, test3);
        familyController.adoptChild(testFamily, test7);
        familyController.adoptChild(testFamily, test6);
        familyController.adoptChild(testFamily, test4);
        familyController.adoptChild(testFamily, test5);
        System.out.println("Family members amount of testFamily is: " + testFamily.countFamily());
        System.out.println("---------------------------------");
        familyController.getFamiliesBiggerThan(2);
        System.out.println("---------------------------------");
        familyController.getFamiliesLessThan(5);
        System.out.println("---------------------------------");
        familyController.getFamiliesLessThan(9);
        System.out.println("---------------------------------");
        familyController.countFamiliesWithMemberNumber(78);
        System.out.println("---------------------------------");
        familyController.countFamiliesWithMemberNumber(7);
        System.out.println("---------------------------------");
        familyController.deleteAllChildrenOlderThan(17);
        System.out.println("---------------------------------");
        System.out.println("Family members amount of testFamily is: " + testFamily.countFamily());

    }
}






